#!/usr/bin/env bash

LIBXIMC_VERSION='2.13.6'

echo "$# arguments passed"

if [[ "$#" > "0" ]] ; then LIBXIMC_VERSION="$1" ; fi

echo "[$0]: Will download and link pyximc from libximc version ${LIBXIMC_VERSION}"
echo "[$0]: Please note: this will not install libximc libraries - this is your responsibility!"
echo "[$0]: This script only locally downloads and links the required python wrapper and profiles."

echo '[0]: Remove old pyximc download and links'
# Remove symlinks
rm -f pyximc.py
rm -f pyximc_profiles
# Remove third-party/ximc
rm -rf ../third-party/ximc-*/

LIBXIMC_ARCHIVE="libximc-${LIBXIMC_VERSION}-all.tar.gz"
echo "[1]: Download libximc distribution ${LIBXIMC_ARCHIVE}"
mkdir -p ../third-party
curl "https://files.xisupport.com/libximc/${LIBXIMC_ARCHIVE}" > "../third-party/${LIBXIMC_ARCHIVE}" || (echo "Could not download ${LIBXIMC_ARCHIVE} from https://files.xisupport.com/libximc/... Giving up!"; exit 1)

echo '[2]: Unpack libximc distribution'
tar -xvzf "../third-party/${LIBXIMC_ARCHIVE}" -C "../third-party/" || (echo "Could not unzip ${LIBXIMC_ARCHIVE}... Giving up!"; exit 1)
rm "../third-party/${LIBXIMC_ARCHIVE}"

echo '[3]: Symlink pyximc.py and pyximc_profiles'
ln -s ../third-party/ximc-*/ximc/crossplatform/wrappers/python/pyximc.py pyximc.py
if [ ! -L pyximc.py ] || [ ! -e pyximc.py ]; then
  echo "Failed to symlink pyximc.py => ../third-party/ximc-*/ximc/crossplatform/wrappers/python/pyximc.py"
fi
ln -s ../third-party/ximc-*/ximc/python-profiles/ pyximc_profiles
if [ ! -L pyximc_profiles ] || [ ! -e pyximc_profiles ]; then
  echo "Failed to symlink pyximc_profiles => ../third-party/ximc-*/ximc/python-profiles"
fi

echo '[4]: Done.'
